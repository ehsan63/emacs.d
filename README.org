* Fonts:
  My settings use the following fonts. They should be installed on the system.
  - Latin: [[https://github.com/be5invis/Iosevka][Iosevka SS09 Source Code Style]]
  - Farsi/Arabic: [[https://github.com/rastikerdar/vazir-font][Vazir]]
  In case you want to change those, edit following lines in ~´customizations/ehsan-appearance.el´~
  #+begin_src emacs-lisp
  (set-fontset-font
     "fontset-default"
     (cons (decode-char 'ucs #x0600) (decode-char 'ucs #x06ff)) ; arabic
     "Vazir Light 11")
  (set-face-font 'default "Iosevka SS09 Light 13")
  #+end_src
    
* Packages:
  - Evil
  - Helm
  - Org-mode
    - The _latex_ =article= *class* '/italic/' for ~APS~ journals
  - dashboard
  - /zz/ rr
