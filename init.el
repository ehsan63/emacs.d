;; Set up package.el to work with MELPA
;; package and use-package setup
(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives '(("org"  . "http://orgmode.org/elpa/")
                        ("gnu"   . "http://elpa.gnu.org/packages/")
			("tromey" . "http://tromey.com/elpa/")
                        ("melpa" . "http://melpa.org/packages/")))

(add-to-list 'package-archives
             '("melpa2" . "http://melpa.milkbox.net/packages/") t)
(add-to-list 'package-archives
             '("melpa-stable" . "http://stable.melpa.org/packages/") t)
(package-initialize)

;; Download the ELPA archive description if needed.
;; This informs Emacs about the latest versions of all packages, and
;; makes them available for download.
(when (not package-archive-contents)
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
;;  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

(defvar my-packages
  '(
    better-defaults
    elpy
    flycheck                        ;; On the fly syntax checking
    py-autopep8                     ;; Run autopep8 on save
    ;; blacken                         ;; Black formatting on 
    ;; makes handling lisp expressions much, much easier
    ;; Cheatsheet: http://www.emacswiki.org/emacs/PareditCheatsheet
    ;; Rust
    rust-mode
    cargo
    ;;lsp-mode
    
    ;;
    paredit
    ;; key bindings and code colorization for Clojure
    ;; https://github.com/clojure-emacs/clojure-mode
    clojure-mode
    ;; extra syntax highlighting for clojure
    clojure-mode-extra-font-locking
    ;; integration with a Clojure REPL
    ;; https://github.com/clojure-emacs/cider
    cider
    ;; project navigation
    projectile
    ;; colorful parenthesis matching
    rainbow-delimiters
    ;; edit html tags like sexps
    tagedit
    ;; git integration
    magit
    ;; Evil
    evil
    ;; pdf-tools
    pdf-tools
    ;;
    org-bullets
    dashboard
    langtool
    ))
(dolist (p my-packages)
    (when (not (package-installed-p p))
	      (package-install p)))

;; Enable Evil
(require 'evil)
(evil-mode 1)

;;--------------------------------------
(pdf-tools-install)
;; more fine-grained zooming
(setq pdf-view-resize-factor 1.1)

;;--------------------------------------

;; setup helm
(use-package helm
  :init
  (setq helm-split-window-default-side 'other)
  (helm-mode 1))

(use-package helm-bibtex
    :custom
    (helm-bibtex-bibliography '("~/zotero.bib"))
    (reftex-default-bibliography '("~/zotero.bib"))
    (bibtex-completion-pdf-field "file")
    :hook (Tex . (lambda () (define-key Tex-mode-map "\C-ch" 'helm-bibtex))))

(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-x C-f") 'helm-find-files)

;; Turn on recent file mode so that you can more easily switch to
;; recently edited files when you first start emacs
(setq recentf-save-file (concat user-emacs-directory ".recentf"))
(require 'recentf)
(recentf-mode 1)
(setq recentf-max-menu-items 40)

;; projectile everywhere!
(projectile-global-mode)

;; Place downloaded elisp files in ~/.emacs.d/vendor. You'll then be able
;; to load them.
;;
;; (require 'yaml-mode)
;; (add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))
;; 
;; Adding this code will make Emacs enter yaml mode whenever you open
;; a .yml file
(add-to-list 'load-path "~/.emacs.d/vendor")

;;;;
;; Customization
;;;;

;; Add a directory to our load path so that when you `load` things
;; below, Emacs knows where to look for the corresponding file.
(add-to-list 'load-path "~/.emacs.d/customizations")

(load "ehsan-org.el")
;; My font settings are in this file:
(load "ehsan-appearance.el")
;(load "ehsan-mail-calender.el")

;; Projectile
(require 'projectile)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
(projectile-mode +1)

;; Dashboard
(require 'dashboard)
(dashboard-setup-startup-hook)

;; langtool
(setq langtool-language-tool-jar "/snap/languagetool/current/usr/bin/languagetool-commandline.jar")
(require 'langtool)
(global-set-key "\C-x4w" 'langtool-check)
(global-set-key "\C-x4W" 'langtool-check-done)
(global-set-key "\C-x4l" 'langtool-switch-default-language)
(global-set-key "\C-x44" 'langtool-show-message-at-point)
(global-set-key "\C-x4c" 'langtool-correct-buffer)
;; ====================================
;; Development Setup
;; ====================================
;;(use-package helm-lsp :commands helm-lsp-workspace-symbol)

;; Magit
(require 'magit)
(global-set-key (kbd "C-x g") 'magit-status)
;;
(require 'rust-mode)
(add-hook 'rust-mode-hook
          (lambda () (setq indent-tabs-mode nil)))
(add-hook 'rust-mode-hook 'cargo-minor-mode)
(setq rust-format-on-save t)
(define-key rust-mode-map (kbd "C-c C-c") 'rust-run)
;; Enable elpy
(elpy-enable)
;; Use IPython for REPL
(setq python-shell-interpreter "jupyter"
      python-shell-interpreter-args "console --simple-prompt"
      python-shell-prompt-detect-failure-warning nil)
(add-to-list 'python-shell-completion-native-disabled-interpreters
             "jupyter")
;; Enable Flycheck
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))
;; Enable autopep8
(require 'py-autopep8)
(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)

;;--------------------------------------------------------
(setq custom-file "~/.emacs.d/emacs-custom.el")
(load custom-file)
