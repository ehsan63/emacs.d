;; Mail and Calendar
;; Set up bbdb
(require 'bbdb)
(bbdb-initialize 'message)
(bbdb-insinuate-message)
(add-hook 'message-setup-hook 'bbdb-insinuate-mail)
;; set up calendar
(require 'calfw)
(require 'calfw-ical)
;; Set this to the URL of your calendar. Google users will use
;; the Secret Address in iCalendar Format from the calendar settings
(cfw:open-ical-calendar "https://calendar.google.com/calendar/ical/ehsan.irani%40gmail.com/private-e43763dfb29a468f4e4f01fc2c2d15a9/basic.ics")
;; Set up notmuch
(require 'notmuch)
;; set up mail sending using sendmail
(setq send-mail-function (quote sendmail-send-it))
(setq user-mail-address "ehsan.irani@mdc-berlin.de"
      user-full-name "Ehsan Irani")