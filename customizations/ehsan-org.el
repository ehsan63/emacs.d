;; Fancy bullets and TODO
;;-----------------------------------------------------;;
(use-package org-bullets
:ensure t
:config
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(setq org-default-notes-file "~/org/tasks.org")
(add-to-list 'org-capture-templates
             '("t" "Personal Task"  entry
               (file org-default-notes-file)
               "* TODO %?" :empty-lines 1))

(add-to-list 'org-capture-templates
             '("w" "Work-related Task"  entry
               (file "~/org/work.org")
               "* TODO %?" :empty-lines 1))

(global-set-key (kbd "C-c o") 
                (lambda () (interactive) (find-file "~/org/organizer.org")))

(setq org-refile-targets '((org-agenda-files . (:maxlevel . 6))))

;;-----------------------------------------------------;;

(use-package org-ref
    :custom
    (org-ref-default-bibliography "~/zotero.bib"))

(setq org-highlight-latex-and-related '(latex))


(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
;; ** <<APS journals>>
(add-to-list 'org-latex-classes '("revtex4-2"
				  "\\documentclass{revtex4-2}
 [NO-DEFAULT-PACKAGES]
 [PACKAGES]
 [EXTRA]"
				  ("\\section{%s}" . "\\section*{%s}")
				  ("\\subsection{%s}" . "\\subsection*{%s}")
				  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
				  ("\\paragraph{%s}" . "\\paragraph*{%s}")
				  ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(with-eval-after-load 'org
  (setq org-display-inline-images t)
  (setq org-redisplay-inline-images t)
  (setq org-startup-with-inline-images "inlineimages")
  (setq org-hide-emphasis-markers t)
  (setq org-confirm-elisp-link-function nil))
;  (setq org-link-frame-setup '((file . find-file))))
